- TODO list for inhomog

* Add some mechanism for checking that required values in the
   structures background_cosm_params and rza_integrand_params have
   been initialised and are valid. [high priority; usability;
   robustness]

* Documentation: doxygenisation. Rough initial draft done, as of
   v0.1.1. [high priority; usability]

* Throwing errors - which suspicious choices should be considered
   warnings vs fatal errors? which should be thrown up through
   the hierarchy of routines using e.g. the <errno.h> library?
   According to https://lintian.debian.org/tags/shlib-calls-exit.html,
   *all* errors, including fatal ones, should be thrown up
   the hierarchy and the C exit() function should not at all be
   used. [medium priority]

* Libtool - in the extended regular expression of the libtool
   option -export-symbols-regex in lib/Makefile.am, restrict
   this to routines ("symbols") that really need to be used
   externally; this will require increasing the V number of
   the library, since this means removing symbols from the
   interface. [low priority]

* Re-read IEEE and GNU guidelines and play with lib/test_gsl_ieee
   to see if gsl ieee modes can be usefully integrated into inhomog.
   [low priority]

- ---------- DONE -------------------------------------------------

* LCDM speed - try out Kasai (2010, arXiv:1012.2671) equation (5)
   for the flat, non-EdS FLRW growth function: use maxima to obtain
   polynomial+sqrt ratios for the first and second derivatives;
   combine this with a_dot_flatFLRW to get the two time derivatives of
   the growth function. Benchmark the Kasai vs Bildhauer et al
   expressions; Kasai also refers to a less accurate Lahav
   approximation used by Carroll. It's not clear if any of these
   alternative expressions have less than about 20-30 operations
   to compute, so the time invested might not improve the speed
   by much. DONE: significant speedups - see notes in git, then in
   ChangeLog or briefly in NEWS.

* LCDM speed - benchmark EdS vs LCDM calculation speed for
   a call to Omega_D. Work out why LCDM seems to be about 80 or so
   times slower than EdS and see if the LCDM case can be sped
   up. FLRW_background.c has analytical formulae for the LCDM (generic
   flatFLRW) a(t), t(a), and \dot{a}(t) functions, so the slowdown for
   LCDM should not need to be so great. 2019-01-05: DONE: the non-EdS
   growth function is the slowdown factor, by a factor of about
   20-30.

* Debianisation - as of version >= 0.1.5 (debian: 0.1.5-1), inhomog
   is available in source code at
   https://anonscm.debian.org/cgit/debian-astro/packages/inhomog.git
   or more generally, see
   http://tracker.debian.org/inhomog for studying Debian warnings
   and errors in the debianised version of inhomog; fixes should
   preferably be done upstream at bitbucket so that they are available
   for all users.
