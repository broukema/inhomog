/*
   parse_noempty_strtox - wrappers for strtod and strtol for command line parsing

   Copyright (C) 2012 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
   See also http://www.gnu.org/licenses/gpl.html

*/

int parse_noempty_strtox(/* INPUTS */
                         char *string,  /* input argument string */
                         const char *error_message_1, /* prefix of error message, e.g. program name */
                         const char *error_message_2, /* e.g. name of command line option */
                         char value_type,     /* 'l' or 'd' */
                         int  fatal_fail,       /* -1 = comment only, 0 = warning only, 1 = force an exit if failure*/
                         /* OUTPUTS */
                         void *valid_value,      /* address of the output value */
                         char ** endptr         /* same as endptr for strtol, strtod */
                    );
