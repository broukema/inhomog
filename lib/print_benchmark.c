/* 
   print_benchmark

   Copyright (C) 2013 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

   See also http://www.gnu.org/licenses/gpl.html
   
*/

#include <stdio.h>
#include <time.h>
#include "lib/inhomog.h"

/* wrapper so that the user doesn't need to remember the clock_t details */
double print_benchmark(clock_t b0, clock_t b1){
  double time_diff_seconds;

  time_diff_seconds = (double)(b1-b0)/(double)CLOCKS_PER_SEC;
  printf("bench: %f\n", time_diff_seconds);

  return time_diff_seconds;
}
