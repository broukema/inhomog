/*
   test kinematic backreaction - (49) of Buchert et al RZA2 preprint v20120722

   Copyright (C) 2013 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*! \file test_kinematical_backreaction.c
 *
 * Allows for an independent check of kinematical_backreaction.c; all
 * the necessary parameters from rza_integrand_params_s and
 * background_cosm_params_s are set before using
 * \ref kinematical_backreaction.
 *
 * Checks for both EdS and FLRW models, as well as for both spherical
 * and non-spherical cases.
 */

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>
#include <gsl/gsl_rng.h>

/* for malloc_usable_size if available */
#ifdef __GNUC__
#include <malloc.h>
#endif

#ifdef _OPENMP
#include <omp.h>
#endif

#include <time.h>

#include "lib/inhomog.h"

/* static variables don't make sense in these functions when
   called by multiple threads; thread-local static variables
   would have to be created */
#ifdef _OPENMP
#define BACKREACTION_STATIC
#else
#define BACKREACTION_STATIC static
#endif

#define DEBUG 1

/* #undef DEBUG */

/* int test_kinematical_backreaction( *//* INPUTS: */
int main(void)
{

#define N_PLOT_T 5
#define N_PLOT_R 2
#define N_COSM 2
  double R_domain_lower_BKS00;
  double R_domain_upper_BKS00;
  double R_domain_mod[N_PLOT_R];

#define N_TESTS 2
  int i_test;
  struct rza_integrand_params_s rza_integrand_params[N_PLOT_R][N_TESTS];
  struct background_cosm_params_s background_cosm_params;

  double t_i[N_COSM]; /* = 0.004574998; */ /* for z=200, H_0=50, EdS */
  double t_0[N_COSM]; /* = 13.04       */  /* for z=200, H_0=50, EdS */
  double t_background[N_COSM][N_PLOT_T];
  double rza_Q_D_spherical[N_COSM][N_PLOT_R][N_PLOT_T];
  double rza_Q_D[N_COSM][N_PLOT_R][N_PLOT_T];
  double delta_t;
  double delta_ln_R;

  const gsl_rng_type * T_gsl;
  gsl_rng * r_gsl;
  static unsigned long int local_gsl_seed=0;

  long   n_calls_invariants = 300000;
  double n_sigma[3] = {1.0, 1.0, 1.0};
  /* int    I_invariant; */

  int i_t, i_R; /* counters in time and length scale */

  int want_planar; /* cf RZA2 V.A */
  int want_spherical;
  int want_verbose=0;
  int i_thread;
  int pass=0;
  int pass_eights = 1;

  /* benchmarking */
  clock_t  benchmark[10]; /* num elements hardwired! */
  int  i_bench=0;

#define QRZA_SPHERICAL_TEST 2e-15
  /*
     The following requires fairly small R_domain values for
     testing, since otherwise numerical variation in the
     Monte Carlog Vegas integration causes too much fluctuation
     to pass the test.
   */
  double q0,q1; /* local only */
  double qrza_eds_vs_lambda_test[N_PLOT_R] =
    {0.03, 0.2};
  /*  {0.05, 0.025}; */

  int i_EdS;

  gsl_rng_env_setup();
  T_gsl = gsl_rng_default;
  gsl_rng_default_seed += 8527 + local_gsl_seed;
  local_gsl_seed = gsl_rng_default_seed;
  r_gsl = gsl_rng_alloc (T_gsl); /* this gets reallocated each time the
                                    function gets called */

  /*
     allow nested openmp if possible - so that the
     FLRW background models can be done in parallel
     #ifdef _OPENMP
     omp_set_nested(1);
     #endif
  */

  for(i_R=0; i_R<N_PLOT_R; i_R++){
    for (i_test=0; i_test<N_TESTS; i_test++){
      /* test standard spherical domains */
      rza_integrand_params[i_R][i_test].w_type = 1;
      /*  used if(2==rza_integrand_params[i_R][i_test]w_type) */
      rza_integrand_params[i_R][i_test].delta_R = DELTA_R_DEFAULT;
      /* BBKS (BKS version) default */
      rza_integrand_params[i_R][i_test].pow_spec_type = 'B';
      rza_integrand_params[i_R][i_test].precalculated_invariants.enabled = 0;
    };
  };


  printf("\ntest_kinematical_backreaction:\n");

  background_cosm_params.flatFLRW = 1;

  background_cosm_params.inhomog_a_scale_factor_initial = 1.0/201.0;
  background_cosm_params.inhomog_a_d_scale_factor_initial = 1.0/201.0;
  background_cosm_params.inhomog_a_scale_factor_now = 1.0;

  benchmark[i_bench] = clock(); /* initialise timer */

  for(i_EdS=0; i_EdS<2; i_EdS++){
    background_cosm_params.EdS = i_EdS;
    printf("background_cosm_params.EdS  = %d\n",
           background_cosm_params.EdS);

    i_bench ++;
    benchmark[i_bench]=clock();
    printf("beginning of i_EdS = %d loop\n",i_EdS);
    print_benchmark(benchmark[i_bench-1],benchmark[i_bench]);

    background_cosm_params.recalculate_t_0 = 1; /* initially recalculate for this test */
    if(1 == background_cosm_params.EdS){
      background_cosm_params.H_0 = 50.0;
      background_cosm_params.Omm_0 = 1.0;
      background_cosm_params.OmLam_0 = 0.0;
      t_i[i_EdS] = t_EdS(&background_cosm_params,
                         background_cosm_params.inhomog_a_scale_factor_initial,
                  want_verbose);
    }else if(1 == background_cosm_params.flatFLRW){
      /*    background_cosm_params.H_0 = 70.0;
            background_cosm_params.OmLam_0 = 0.70; */
      background_cosm_params.H_0 = 50.0;
      background_cosm_params.OmLam_0 = 0.001;
      background_cosm_params.Omm_0 = 1.0 - background_cosm_params.OmLam_0;
      t_i[i_EdS] = t_flatFLRW(&background_cosm_params,
                              background_cosm_params.inhomog_a_scale_factor_initial,
                       want_verbose);
    }else{
      printf("No other options for background_cosm_params so far in program.\n");
      exit(1);
    };

    background_cosm_params.recalculate_t_0 = 1;


    /* initial physical units =>  5 Mpc comoving */
    R_domain_lower_BKS00 =
      0.5 * 10.0*
      background_cosm_params.inhomog_a_scale_factor_initial /
      background_cosm_params.inhomog_a_scale_factor_now;
    /* initial physical units => 25 Mpc comoving */
    R_domain_upper_BKS00 =
      0.5 * 50.0*
      background_cosm_params.inhomog_a_scale_factor_initial
      / background_cosm_params.inhomog_a_scale_factor_now;

    t_0[i_EdS] = background_cosm_params.t_0;
    delta_t = (t_0[i_EdS]-t_i[i_EdS])/((double)(N_PLOT_T-1));

    delta_ln_R = (log(R_domain_upper_BKS00) - log(R_domain_lower_BKS00))/(double)(N_PLOT_R-1);
    for(i_R=0; i_R<N_PLOT_R; i_R++){
      R_domain_mod[i_R] = exp( log(R_domain_lower_BKS00) + ((double)i_R)* delta_ln_R );
    };


    /* random (50:50) choice of + or - fluctuations, independently for
       each of the three invariants  */

    /* not used in RZA2 article:
       for(I_invariant=0; I_invariant<3; I_invariant++){
       if(gsl_rng_uniform(r_gsl) > 0.5)
       n_sigma[I_invariant] = -n_sigma[I_invariant];
       };
    */

    for(i_t=0; i_t<N_PLOT_T; i_t++){
      t_background[i_EdS][i_t] = t_i[i_EdS] + (double)i_t * delta_t;
    };


#pragma omp parallel                            \
  default(shared)                               \
  shared(pass)                                  \
  private(i_R,i_test,i_thread,                  \
          want_spherical,want_planar)           \
  firstprivate(R_domain_mod,                    \
               rza_integrand_params,            \
               background_cosm_params)
    {
#pragma omp for schedule(dynamic)
      for(i_R=0; i_R<N_PLOT_R; i_R++){
        for (i_test=0; i_test<N_TESTS; i_test++){

          switch(rza_integrand_params[i_R][i_test].w_type)
            {
            case 1:
            default:
              rza_integrand_params[i_R][i_test].R_domain = R_domain_mod[i_R];
              break;

            case 2:
              rza_integrand_params[i_R][i_test].R_domain_1 = R_domain_mod[i_R] -
                0.5*rza_integrand_params[i_R][i_test].delta_R;
              rza_integrand_params[i_R][i_test].R_domain_2 = R_domain_mod[i_R] +
                0.5*rza_integrand_params[i_R][i_test].delta_R;
              break;
            };

          /* The invariants have to be calculated; they are not yet known. */
          rza_integrand_params[i_R][i_test].sigma_sq_inv_triple.I_known = 0;
          rza_integrand_params[i_R][i_test].sigma_sq_inv_triple.II_known = 0;
          rza_integrand_params[i_R][i_test].sigma_sq_inv_triple.III_known = 0;

#ifdef _OPENMP
          i_thread= omp_get_thread_num();
#else
          i_thread=0;
#endif

          /* test Q_D itself */
          /* spherical test: cf RZA2 V.B.3  */
          want_planar=0;
          if(0==i_test){
            want_spherical=1;
          }else{
            want_spherical=0;
          };

          printf("thread%d: want_spherical = %d\n",
                 i_thread,want_spherical);
          if(0==i_test){
            kinematical_backreaction(&(rza_integrand_params[i_R][i_test]),
                                     background_cosm_params,
                                     (double *)&(t_background[i_EdS][0]), (int)1,
                                     n_sigma,
                                     n_calls_invariants,
                                     want_planar, /* cf RZA2 V.A */
                                     want_spherical,
                                     want_verbose,
                                     (double*)&(rza_Q_D_spherical[i_EdS][i_R][0])
                                     );
          }else{
            kinematical_backreaction(&(rza_integrand_params[i_R][i_test]),
                                     background_cosm_params,
                                     (double *)&(t_background[i_EdS][0]), (int)N_PLOT_T,
                                     n_sigma,
                                     n_calls_invariants,
                                     want_planar, /* cf RZA2 V.A */
                                     want_spherical,
                                     want_verbose,
                                     (double*)&(rza_Q_D[i_EdS][i_R][0])
                                     );
          };

          if(0==i_test){
            printf("thread%d: R, spherical rza_Q_D_spherical[i_EdS][i_R][0] = %g %g \n",
                   i_thread,
                   R_domain_mod[i_R],
                   rza_Q_D_spherical[i_EdS][i_R][0] );
            if(!isfinite(fabs(rza_Q_D_spherical[i_EdS][i_R][0])) ||
               fabs(rza_Q_D_spherical[i_EdS][i_R][0]) > QRZA_SPHERICAL_TEST){
              pass += 1 *pass_eights;
              printf("ERROR: fabs(rza_Q_D_spherical[i_EdS][i_R][0]) = %g, QRZA_SPHERICAL_TEST = %g\n",
                     fabs(rza_Q_D_spherical[i_EdS][i_R][0]), QRZA_SPHERICAL_TEST);
            };
          };

        }; /* for (i_test=0; i_test<N_TESTS; i_test++) */
      };  /*    for(i_R=0; i_R<N_PLOT_R; i_R++) */
    }; /* #pragma omp parallel                            */


    for(i_R=0; i_R<N_PLOT_R; i_R++){
      for(i_t=0; i_t<N_PLOT_T; i_t++){
        printf("R t rza_Q_D: %g %g %g\n",
               R_domain_mod[i_R],
               t_background[i_EdS][i_t],
               rza_Q_D[i_EdS][i_R][i_t]);
      };
    };
    pass_eights *= 8;

    i_bench ++;
    benchmark[i_bench]=clock();
    printf("...this test took: ");
    print_benchmark(benchmark[i_bench-1],benchmark[i_bench]);

  }; /*   for(i_EdS=0; i_EdS<2; i_EdS++) */

  i_bench ++;
  benchmark[i_bench]=clock();
  printf("...the last few lines took: ");
  print_benchmark(benchmark[i_bench-1],benchmark[i_bench]);

  printf("tcalc_LCDM/tcalc_EdS: %4u %4u %8ld %7.2f \n",
         (uint)N_PLOT_T,(uint)N_PLOT_R,n_calls_invariants,
         (double)(benchmark[2]-benchmark[1])/
         (double)(benchmark[4]-benchmark[3]));


  for(i_R=0; i_R<N_PLOT_R; i_R++){
    q0 = rza_Q_D[0][i_R][N_PLOT_T-1];
    q1 = rza_Q_D[1][i_R][N_PLOT_T-1];
    printf("test_kinematical_backreaction:");
    if(  !isfinite(fabs((q0-q1)*0.5/(q0+q1))) ||
         fabs((q0-q1)*0.5/(q0+q1)) > qrza_eds_vs_lambda_test[i_R] ){
      pass += (2+i_R) * pass_eights;
      printf(" ERROR: ");
      printf("fabs((q0-q1)*0.5/(q0+q1)) = %g >  qrza_eds_vs_lambda_test[%d] = %g\n",
             fabs((q0-q1)*0.5/(q0+q1)),
             i_R, qrza_eds_vs_lambda_test[i_R]);
    }else{
      printf("\n");
    };
    printf(" final Q_D EdS vs nearly-EdS at R = %g relative diff = %g ; cf %g\n",
           R_domain_mod[i_R], fabs((q0-q1)*0.5/(q0+q1)),
           qrza_eds_vs_lambda_test[i_R]);
    printf(" Q_D = %g %g\n",q0, q1);
  };

  gsl_rng_free(r_gsl);

  printf("test_kinematical_backreaction: pass = %d\n",
         pass);
  return pass;
}
