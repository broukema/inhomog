/*
   alloc_big_array - allocate or free big multi-dimensional arrays

   Copyright (C) 2014-2017 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <inttypes.h>
#include <stdint.h>
#include <math.h>
#include <errno.h>

#include "lib/alloc_big_array.h"

#ifdef __linux__
#include <sys/sysinfo.h>
/* max_percent_RAM should be e.g. 95 or 70 */
#define max_percent_RAM 95
#endif

/* e.g. for malloc_usable_size() ... */
#if defined(__GNUC__) && !defined(_MALLOC_H)
#include <malloc.h>
#endif

/* possible TODO: consider using the mdarrays (GPLv2) library
   http://danielwebb.us/software/mdarrays/
*/


/* TODO: privatise the prototype for this: */
int report_out_of_mem(uint64_t available,
                      uint64_t needed,
                      const char  alloc_type[5]){
  printf("alloc_big_array_%s Error: %" PRIu64 " Mb available but need %" PRIu64 " Mb\n",
         alloc_type, available/1048576, needed/1048576);
  return 0;
}

/* private prototype */
int report_request_too_much_mem(uint64_t pqrs,
                                const char  alloc_type[5]);

int report_request_too_much_mem(uint64_t pqrs,
                                const char  alloc_type[5]){
  printf("alloc_big_array_%s: Error converting %" PRIu64 " to size_t type (of size %" PRIu64 " bytes).\n",
         alloc_type,
         pqrs,
         (uint64_t)(sizeof(size_t)));
  return 0;
}

int alloc_big_array_1D_d(uint64_t p,
                         double ** array_1D,
                         int initialise_to_zero){
  uint64_t i;

#ifdef __linux__
  uint64_t mem_needed, pdouble;
  struct sysinfo info;
#endif

#ifdef __linux__
  pdouble = sizeof(double*);
  mem_needed = p*sizeof(double) + pdouble;

  /* assumes linux >= 2.3.48 */
  sysinfo(&info);
  if(mem_needed > (uint64_t)( (double)max_percent_RAM *0.01 *
                            (double)(info.freeram * info.mem_unit) )
     ){
    report_out_of_mem(info.freeram * info.mem_unit,
                      mem_needed,
                      "1D_d");
    return ENOMEM;
  };
#endif

  if((size_t)p - p != 0){
    report_request_too_much_mem(p, "1D_d");
    return ENOMEM;
  };

  *array_1D = malloc((size_t)p*sizeof(double));
  if(*array_1D==NULL){
    return ENOMEM;
  }else if(initialise_to_zero){
    for(i=0; i<p; i++){
      (*array_1D)[i] = 0.0;
    };
  };

  return 0;
}

int free_big_array_1D_d(double * array_1D){
  free(array_1D);
  return 0;
}


int alloc_big_array_2D_d(uint64_t n,
                         uint64_t p,
                         double *** array_2D,
                         int initialise_to_zero){
  uint64_t i;
  uint64_t j;

#ifdef __linux__
  uint64_t mem_needed, pdouble;
  struct sysinfo info;
#endif

#ifdef __linux__
  pdouble = sizeof(double*);
  mem_needed = n*(p*sizeof(double) +
                  pdouble)+ pdouble;

  /* assumes linux >= 2.3.48 */
  sysinfo(&info);
  if(mem_needed > (uint64_t)( (double)max_percent_RAM *0.01 *
                            (double)(info.freeram * info.mem_unit) )
     ){
    report_out_of_mem(info.freeram * info.mem_unit,
                      mem_needed,
                      "2D_d");
    return ENOMEM;
  };
#endif

  if((size_t)p - p != 0){
    report_request_too_much_mem(p, "2D_d");
    return ENOMEM;
  };
  if((size_t)n - n != 0){
    report_request_too_much_mem(n, "2D_d");
    return ENOMEM;
  };


  *array_2D = malloc((size_t)n*sizeof(double *));
  if(*array_2D==NULL){
    return ENOMEM;
  };
  for(i=0; i<n; i++){
    (*array_2D)[i] = malloc((size_t)p*sizeof(double));
    if((*array_2D)[i]==NULL){
      return ENOMEM;
    }else if(initialise_to_zero){
      for(j=0; j<p; j++){
        (*array_2D)[i][j] = 0.0;
      };
    };
  };

  return 0;
}

int free_big_array_2D_d(uint64_t n,
                        double ** array_2D){
  uint64_t i;
  if(NULL!=array_2D) for(i=0; i<n; i++){
      free(array_2D[i]);
    };
  free(array_2D);

  return 0;
}


int alloc_big_array_3D_d(uint64_t m,
                         uint64_t n,
                         uint64_t p,
                         double **** array_3D,
                         int initialise_to_zero){

  uint64_t i,j,k;

#ifdef __linux__
  uint64_t mem_needed, pdouble;
  struct sysinfo info;
#endif

#ifdef __linux__
  pdouble = sizeof(double*);
  mem_needed = m*(n*(p*sizeof(double) +
                     pdouble)+
                  pdouble)+ pdouble;

  /* assumes linux >= 2.3.48 */
  sysinfo(&info);
  if(mem_needed > (uint64_t)( (double)max_percent_RAM *0.01 *
                            (double)(info.freeram * info.mem_unit) )
     ){
    report_out_of_mem(info.freeram * info.mem_unit,
                      mem_needed,
                      "3D_d");
    return ENOMEM;
  };
#endif

  if((size_t)p - p != 0){
    report_request_too_much_mem(p, "3D_d");
    return ENOMEM;
  };
  if((size_t)n - n != 0){
    report_request_too_much_mem(n, "3D_d");
    return ENOMEM;
  };
  if((size_t)m - m != 0){
    report_request_too_much_mem(m, "3D_d");
    return ENOMEM;
  };


  *array_3D = malloc((size_t)m*sizeof(double **));
  if(*array_3D==NULL){
    return ENOMEM;
  };
  for(i=0; i<m; i++){
    (*array_3D)[i] = malloc((size_t)n*sizeof(double *));
    if((*array_3D)[i]==NULL){
      return ENOMEM;
    };
    for(j=0; j<n; j++){
      (*array_3D)[i][j] = malloc((size_t)p*sizeof(double));
      if((*array_3D)[i][j]==NULL){
        return ENOMEM;
      }else if(initialise_to_zero){
        for(k=0; k<p; k++){
          (*array_3D)[i][j][k] = 0.0;
        };
      };

    };
  };

  return 0;
}

int free_big_array_3D_d(uint64_t m,
                        uint64_t n,
                        double *** array_3D){
  uint64_t i,j;

  if(NULL!=array_3D) for(i=0; i<m; i++){
      if(NULL!=array_3D[i]) for(j=0; j<n; j++){
          free(array_3D[i][j]);
        };
      free(array_3D[i]);
    };
  free(array_3D);

  return 0;
}



int alloc_big_array_4D_d(uint64_t m,
                         uint64_t n,
                         uint64_t p,
                         uint64_t q,
                         double ***** array_4D,
                         int initialise_to_zero){

  uint64_t i,j,k,l;

#ifdef __linux__
  uint64_t mem_needed, pdouble;
  struct sysinfo info;
#endif

#ifdef __linux__
  pdouble = sizeof(double*);
  mem_needed = m*(n*(p*(q*sizeof(double) +
                        pdouble)+
                     pdouble)+
                  pdouble)+ pdouble;

  /* assumes linux >= 2.3.48 */
  sysinfo(&info);
  if(mem_needed > (uint64_t)( (double)max_percent_RAM *0.01 *
                            (double)(info.freeram * info.mem_unit) )
     ){
    report_out_of_mem(info.freeram * info.mem_unit,
                      mem_needed,
                      "4D_d");
    return ENOMEM;
  };
#endif

  if((size_t)q - q != 0){
    report_request_too_much_mem(q, "4D_d");
    return ENOMEM;
  };
  if((size_t)p - p != 0){
    report_request_too_much_mem(p, "4D_d");
    return ENOMEM;
  };
  if((size_t)n - n != 0){
    report_request_too_much_mem(n, "4D_d");
    return ENOMEM;
  };
  if((size_t)m - m != 0){
    report_request_too_much_mem(m, "4D_d");
    return ENOMEM;
  };

  *array_4D = malloc((size_t)m*sizeof(double **));
  if(*array_4D==NULL){
    return ENOMEM;
  };
  for(i=0; i<m; i++){
    (*array_4D)[i] = malloc((size_t)n*sizeof(double *));
    if((*array_4D)[i]==NULL){
      return ENOMEM;
    };
    for(j=0; j<n; j++){
      (*array_4D)[i][j] = malloc((size_t)p*sizeof(double *));
      if((*array_4D)[i][j]==NULL){
        return ENOMEM;
      };
      for(k=0; k<p; k++){
        (*array_4D)[i][j][k] = malloc((size_t)q*sizeof(double));
        if((*array_4D)[i][j][k]==NULL){
          return ENOMEM;
        }else if(initialise_to_zero){
          for(l=0; l<q; l++){
            (*array_4D)[i][j][k][l] = 0.0;
          };
        };  /* if((*array_4D)[i][j][k]==NULL) */

      }; /* for(k=0; k<p; k++) */
    }; /* for(j=0; j<n; j++) */
  }; /*   for(i=0; i<m; i++) */

  return 0;
}

int free_big_array_4D_d(uint64_t m,
                        uint64_t n,
                        uint64_t p,
                        double **** array_4D){
  uint64_t i,j,k;

  if(NULL!=array_4D) for(i=0; i<m; i++){
      if(NULL!=array_4D[i]) for(j=0; j<n; j++){
          if(NULL!=array_4D[i][j]) for(k=0; k<p; k++){
              free(array_4D[i][j][k]);
            };
          free(array_4D[i][j]);
        };
      free(array_4D[i]);
    };
  free(array_4D);

  return 0;
}



int alloc_big_array_2D_i(uint64_t n,
                         uint64_t p,
                         int *** array_2D,
                         int initialise_to_zero){
  uint64_t i;
  uint64_t j;

#ifdef __linux__
  uint64_t mem_needed, pint;
  struct sysinfo info;
#endif

#ifdef __linux__
  pint = sizeof(int*);
  mem_needed = n*(p*sizeof(int) +
                  pint)+ pint;

  /* assumes linux >= 2.3.48 */
  sysinfo(&info);
  if(mem_needed > (uint64_t)( (double)max_percent_RAM *0.01 *
                            (double)(info.freeram * info.mem_unit) )
     ){
    report_out_of_mem(info.freeram * info.mem_unit,
                      mem_needed,
                      "2D_i");
    return ENOMEM;
  };
#endif

  if((size_t)p - p != 0){
    report_request_too_much_mem(p, "2D_i");
    return ENOMEM;
  };
  if((size_t)n - n != 0){
    report_request_too_much_mem(n, "2D_i");
    return ENOMEM;
  };


  *array_2D = malloc((size_t)n*sizeof(int *));
  if(*array_2D==NULL){
    return ENOMEM;
  };
  for(i=0; i<n; i++){
    (*array_2D)[i] = malloc((size_t)p*sizeof(int));
    if((*array_2D)[i]==NULL){
      return ENOMEM;
    }else if(initialise_to_zero){
      for(j=0; j<p; j++){
        (*array_2D)[i][j] = 0;
      };
    };
  };

  return 0;
}

int free_big_array_2D_i(uint64_t n,
                        int ** array_2D){
  uint64_t i;

  if(NULL!=array_2D) for(i=0; i<n; i++){
      free(array_2D[i]);
    };
  free(array_2D);

  return 0;
}


int alloc_big_array_3D_i(uint64_t m,
                         uint64_t n,
                         uint64_t p,
                         int **** array_3D,
                         int initialise_to_zero){

  uint64_t i,j,k;

#ifdef __linux__
  uint64_t mem_needed, pint;
  struct sysinfo info;
#endif

#ifdef __linux__
  pint = sizeof(int*);
  mem_needed = m*(n*(p*sizeof(int) +
                     pint)+
                  pint)+ pint;


  /* assumes linux >= 2.3.48 */
  sysinfo(&info);
  if(mem_needed > (uint64_t)( (double)max_percent_RAM *0.01 *
                            (double)(info.freeram * info.mem_unit) )
     ){
    report_out_of_mem(info.freeram * info.mem_unit,
                      mem_needed,
                      "3D_i");
    return ENOMEM;
  };
#endif

  if((size_t)p - p != 0){
    report_request_too_much_mem(p, "3D_i");
    return ENOMEM;
  };
  if((size_t)n - n != 0){
    report_request_too_much_mem(n, "3D_i");
    return ENOMEM;
  };
  if((size_t)m - m != 0){
    report_request_too_much_mem(m, "3D_i");
    return ENOMEM;
  };


  *array_3D = malloc((size_t)m*sizeof(int **));
  if(*array_3D==NULL){
    return ENOMEM;
  };
  for(i=0; i<m; i++){
    (*array_3D)[i] = malloc((size_t)n*sizeof(int *));
    if((*array_3D)[i]==NULL){
      return ENOMEM;
    };
    for(j=0; j<n; j++){
      (*array_3D)[i][j] = malloc((size_t)p*sizeof(int));
      if((*array_3D)[i][j]==NULL){
        return ENOMEM;
      }else if(initialise_to_zero){
        for(k=0; k<p; k++){
          (*array_3D)[i][j][k] = 0;
        };
      };

    };
  };

  return 0;
}

int free_big_array_3D_i(uint64_t m,
                        uint64_t n,
                        int *** array_3D){
  uint64_t i,j;

  if(NULL!=array_3D) for(i=0; i<m; i++){
      if(NULL!=array_3D[i]) for(j=0; j<n; j++){
          free(array_3D[i][j]);
        };
      free(array_3D[i]);
    };
  free(array_3D);

  return 0;
}


int alloc_big_array_4D_i(uint64_t m,
                         uint64_t n,
                         uint64_t p,
                         uint64_t q,
                         int ***** array_4D,
                         int initialise_to_zero){

  uint64_t i,j,k,l;

#ifdef __linux__
  uint64_t mem_needed, pint;
  struct sysinfo info;
#endif

#ifdef __linux__
  pint = sizeof(int*);
  mem_needed = m*(n*(p*(q*sizeof(int) +
                        pint)+
                     pint)+
                  pint)+ pint;

  /* assumes linux >= 2.3.48 */
  sysinfo(&info);
  if(mem_needed > (uint64_t)( (double)max_percent_RAM *0.01 *
                            (double)(info.freeram * info.mem_unit) )
     ){
    report_out_of_mem(info.freeram * info.mem_unit,
                      mem_needed,
                      "4D_i");
    return ENOMEM;
  };
#endif

  if((size_t)q - q != 0){
    report_request_too_much_mem(q, "4D_i");
    return ENOMEM;
  };
  if((size_t)p - p != 0){
    report_request_too_much_mem(p, "4D_i");
    return ENOMEM;
  };
  if((size_t)n - n != 0){
    report_request_too_much_mem(n, "4D_i");
    return ENOMEM;
  };
  if((size_t)m - m != 0){
    report_request_too_much_mem(m, "4D_i");
    return ENOMEM;
  };

  *array_4D = malloc((size_t)m*sizeof(int **));
  if(*array_4D==NULL){
    return ENOMEM;
  };
  for(i=0; i<m; i++){
    (*array_4D)[i] = malloc((size_t)n*sizeof(int *));
    if((*array_4D)[i]==NULL){
      return ENOMEM;
    };
    for(j=0; j<n; j++){
      (*array_4D)[i][j] = malloc((size_t)p*sizeof(int *));
      if((*array_4D)[i][j]==NULL){
        return ENOMEM;
      };
      for(k=0; k<p; k++){
        (*array_4D)[i][j][k] = malloc((size_t)q*sizeof(int));
        if((*array_4D)[i][j][k]==NULL){
          return ENOMEM;
        }else if(initialise_to_zero){
          for(l=0; l<q; l++){
            (*array_4D)[i][j][k][l] = 0;
          };
        };  /* if((*array_4D)[i][j][k]==NULL) */

      }; /* for(k=0; k<p; k++) */
    }; /* for(j=0; j<n; j++) */
  }; /*   for(i=0; i<m; i++) */

  return 0;
}

int free_big_array_4D_i(uint64_t m,
                        uint64_t n,
                        uint64_t p,
                        int **** array_4D){
  uint64_t i,j,k;

  if(NULL!=array_4D) for(i=0; i<m; i++){
      if(NULL!=array_4D[i]) for(j=0; j<n; j++){
          if(NULL!=array_4D[i][j]) for(k=0; k<p; k++){
              free(array_4D[i][j][k]);
            };
          free(array_4D[i][j]);
        };
      free(array_4D[i]);
    };
  free(array_4D);

  return 0;
}
