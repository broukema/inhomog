/*
   alloc_big_array - allocate or free big multi-dimensional arrays

   Copyright (C) 2014-2018 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdint.h>

#ifndef _MATH_H
#include <math.h>
#endif

/* #include "lib/superclusbao.h" */
#include "lib/alloc_big_array.h"
#include "config.h"

/* e.g. for malloc_usable_size() ... */
#if defined(__GNUC__) && !defined(_MALLOC_H)
#include <malloc.h>
#endif

/* uint64_t: Although these _BIG numbers cannot be too big for routine
   testing (and as dimensions successively go above 1, must be
   successively smaller even on machines with huge memories), they are
   used in allocation routines, which require 64-bit integers for
   sizes. */
#define M_BIG ((uint64_t)10)
#define N_BIG ((uint64_t)10)
#define P_BIG ((uint64_t)100)
#define Q_BIG ((uint64_t)100)
/* #define Q_BIG ((uint64_t)150000) */

int main(void){

  double * big_array_1D;
  double ** big_array_2D;
  double *** big_array_3D;
  double **** big_array_4D;
  unsigned int i=0,j=0,k=0,l=0;
  int initialise_to_zero = 1;
  int allocation_OK;
  int sum=0;
  int pass=0;

  /* 1D */
  allocation_OK= alloc_big_array_1D_d(P_BIG,
                                      &big_array_1D,
                                      initialise_to_zero);
  printf("\nwill allocate big 1D array..., return val = %d\n",
         allocation_OK);
  if(0==allocation_OK){
    for(k=0;k<P_BIG;k++){
      big_array_1D[k] = k+1;
    };
    for(k=0;k<P_BIG;k++){
      sum += (int)big_array_1D[k];
    };
    printf("Gauss' school sum is %d\n", sum);

    printf("will deallocate big 1D array..., return val = %d\n",
           free_big_array_1D_d(big_array_1D));
    if(sum!=5050){
      pass+=1;
    }
  }else{
    pass+=16;
  };

  /* 2D */
  allocation_OK = alloc_big_array_2D_d(N_BIG,P_BIG,
                                       &big_array_2D,
                                       initialise_to_zero);
  printf("will allocate big 2D array..., return val = %d\n",
         allocation_OK);
  if(0==allocation_OK){
    for(k=0;k<P_BIG;k++){
      big_array_2D[j][k] = k+1;
    };
    sum=0;
    for(k=0;k<P_BIG;k++){
      sum += (int)big_array_2D[j][k];
    };
    printf("Gauss' school sum is %d\n", sum);

    printf("will deallocate big 2D array..., return val = %d\n",
           free_big_array_2D_d(N_BIG,
                               big_array_2D));
    if(sum!=5050){
      pass+=2;
    };
  }else{
    pass+=32;
  };



  /* 3D */
  allocation_OK = alloc_big_array_3D_d(M_BIG,N_BIG,P_BIG,
                                       &big_array_3D,
                                       initialise_to_zero);
  printf("will allocate big 3D array..., return val = %d\n",
         allocation_OK);
  if(0==allocation_OK){
    for(k=0;k<P_BIG;k++){
      big_array_3D[i][j][k] = k+1;
    };
    sum=0;
    for(k=0;k<P_BIG;k++){
      sum += (int)big_array_3D[i][j][k];
    };
    printf("Gauss' school sum is %d\n", sum);

    printf("will deallocate big 3D array..., return val = %d\n",
           free_big_array_3D_d(M_BIG,N_BIG,
                               big_array_3D));
    if(sum!=5050){
      pass+=4;
    }
  }else{
    pass+=64;
  };


  /* 4D */
  allocation_OK = alloc_big_array_4D_d(M_BIG,N_BIG,P_BIG,Q_BIG,
                                       &big_array_4D,
                                       initialise_to_zero);
  printf("will allocate big 4D array..., return val = %d\n",
         allocation_OK);
  if(0==allocation_OK){
    k=0;
    for(l=0;l<P_BIG;l++){
      big_array_4D[i][j][k][l] = l+1;
    };
    sum=0;
    for(l=0;l<P_BIG;l++){
      sum += (int)big_array_4D[i][j][k][l];
    };
    printf("Gauss' school sum is %d\n", sum);

    printf("will deallocate big 4D array..., return val = %d\n",
           free_big_array_4D_d(M_BIG,N_BIG,P_BIG,
                               big_array_4D));
    if(sum!=5050){
      pass+=8;
    }
  }else{
    pass+=128;
  };


  printf("pass = %d\n",pass);

  return pass;
}
