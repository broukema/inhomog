#!/bin/bash

DATE="date +%Y%m%d"
SIGS=1.0
#SIGS="1.0 2.0"

function kbs_cases {
    for ((I_KBS=0; I_KBS<7; I_KBS++)); do
	for SIG in ${SIGS}; do
	    CFLAGS="${CFLAGS} -${1}INHOM_A_D_DOT_INITIAL_BKS -DINHOM_A_D_DOT_INITIAL_CURV_FACTOR=${2} -DI_KBS_CASE=${I_KBS} -DN_SIGMA_SIGMA=${SIG}" ./configure && make clean && make
	    OMP_NUM_THREADS=4 OMP_THREAD_LIMIT=4 ./lib/Omega_D_example > OmD_examp_${DATE}_BKS${1}_CURV${2}_evRay_i_kbs${I_KBS}_sig${SIG}.log
	done
    done
}

kbs_cases D 1
kbs_cases U 1
kbs_cases U 0


